<?php
class Product_model extends CI_Model{
function __construct()  
 {  
  // Call the Model constructor  
  parent::__construct();  
 }
//we will use the select function  


 
/* Function get dynamic data when send all info ($condition,$tableName)*/ 
public function get_conditionData($data,$tableName)
 {
	
   $this->db->select('*');
   $this->db->from($tableName);
   $this->db->where($data);
   $query = $this->db->get();
    //echo $this->db->last_query();
  return  $FinalData =  $query->row_array();
   
 }
 

public function getConditionalRecords($tab, $condi, $orderBy=NULL, $lim=NULL)
 {
	
   $this->db->select('*');
   $this->db->from($tab);
   $this->db->where($condi);
   $query = $this->db->get();
   $resultSet=$query->result();
   
	return $resultSet;
  }
  
  
  //Get Conditational Record Group By 
  
  public function getConditionalRecordsGroupBy($tab, $condi, $groupBy=NULL, $order=NULL)
 {
	
   $this->db->select('*');
   $this->db->from($tab);
   $this->db->where($condi);
   if(!empty($groupBy)){
   $this->db->group_by($groupBy); 
   }
   $query = $this->db->get();
   $resultSet=$query->result();
   
	return $resultSet;
  }
  
 
 
public function updateConditionalRecords($tblName, $data, $conditionArray)
 {
  $this->db->where($conditionArray);
  $res = $this->db->update($tblName, $data); 
  return $res; 
 }

public function delete($table, $condition)
 {
   $this->db->where($condition);
   $delete = $this->db->delete($table);
   return $delete;
 }

public function getRecord($table)  
 {
  $this->db->select('*');
  $this->db->from($table); 
  $query = $this->db->get();
  return $query->result();
 } 
public function getRecordOrderBy($table)  
 {
  $this->db->select('*');
  $this->db->from($table); 
  $this->db->order_by('id'); 
  $query = $this->db->get();
  return $query->result();
 } 
public function getRecordByPagination($table,$limit,$start)  
 {
  $this->db->select('*');
  $this->db->from($table); 
  $this->db->order_by("order_id"); 
  $this->db->order_by("id DESC");
  $this->db->limit($limit, $start);
  $query = $this->db->get();
  $result=$query->result();
  return $result;
 } 

public function getRecordByKeyword($tab, $keyword)  
 {
   if(isset($keyword) and count($keyword)>0)
    {
	  $condStr=array();
	  foreach($keyword as $kj=>$Vj)
	   {
		 $condStr[]= '`'.$kj.'` LIKE "%'.$Vj.'%"';
	   }
	  $cnStr= implode(' or ', $condStr);
	  $query = $this->db->query("SELECT * FROM `".$tab."` WHERE ".$cnStr."");
      $result=$query->result();
      return $result;
	}
  else
   {
	 return false;
	}
 }

public function setRecord($table, $data) 
 {
  $insert = $this->db->insert($table, $data);
  return $this->db->insert_id(); 
 }

public function updateRecord($table,$data,$condition)// update ordering of listing
 {
   $this->db->where('id', $condition);
   $update = $this->db->update($table, $data); 
   return $update; 
 }

public function updateStyleImageList($table,$data,$conditionArr) 
 {
  $this->db->where($condition);
  $update = $this->db->update($table, $data); 
  return $update; 
 }

public function trucateTable($table) 
 {
  $res = $this->db->truncate($table);
  return $res;
 }
 
 /*=====================Shopping Cart ====================*/
  public function get($where)
    {
        $this->db->select('*');
        $query = $this->db->get_where('payments', $where);
        return $query->row();
    }
	   public function update($data,$id){
        $this->db->where('id', $id);
        if ($this->db->update('payments', $data)) {
            return true;
        }

        return false;
    }

    public function getAllUsersWithPropertyType($data)
    {
      $query = $this->db->query("SELECT * FROM `aauth_users` WHERE `type_of_property` = '$data'");
      $result=$query->result();
      return $result;
    }

    public function insertServiceCharge($id, $meter, $product_id, $values)
    {
      $query = $this->db->query("SELECT * FROM  `service_charges` WHERE `meter_no` = '$meter'");
      if ($query->num_rows() >= 1){
        return false;
      }else{
        $query = $this->db->query("INSERT INTO `service_charges` (`user_id`, `meter_no`, `product_id`, `amount_due`) VALUES ('$id', '$meter', '$product_id', '$values')");
        return $query;
      }
      
    }

    public function insertSecurityLevy($id, $meter, $product_id, $values)
    {
      $query = $this->db->query("SELECT * FROM  `security_levy` WHERE `meter_no` = '$meter'");
      if ($query->num_rows() >= 1){
        return false;
      }else{
        $query = $this->db->query("INSERT INTO `security_levy` (`user_id`, `meter_no`, `product_id`, `amount_due`) VALUES ('$id', '$meter', '$product_id', '$values')");
        return $query;
      }
      
    }

    public function insertRefuseBinExtLevy($id, $meter, $product_id, $values)
    {
      $query = $this->db->query("SELECT * FROM  `refuse_bin_levy` WHERE `meter_no` = '$meter'");
      if ($query->num_rows() >= 1){
        return false;
      }else{
        $query = $this->db->query("INSERT INTO `refuse_bin_levy` (`user_id`, `meter_no`, `product_id`, `amount_due`) VALUES ('$id', '$meter', '$product_id', '$values')");
        return $query;
      }
      
    }

    public function insertPowerPayment($id, $meter, $product_id, $values)
    {
      $query = $this->db->query("SELECT * FROM  `power_payment` WHERE `meter_no` = '$meter'");
      if ($query->num_rows() >= 1){
        return false;
      }else{
        $query = $this->db->query("INSERT INTO `power_payment` (`user_id`, `meter_no`, `product_id`, `amount_due`) VALUES ('$id', '$meter', '$product_id', '$values')");
        return $query;
      }
      
    }

    public function insertInfrastructureLevy($id, $meter, $product_id, $values)
    {
      $query = $this->db->query("SELECT * FROM  `infrastructure_levy` WHERE `meter_no` = '$meter'");
      if ($query->num_rows() >= 1){
        return false;
      }else{
        $query = $this->db->query("INSERT INTO `infrastructure_levy` (`user_id`, `meter_no`, `product_id`, `amount_due`) VALUES ('$id', '$meter', '$product_id', '$values')");
        return $query;
      }
      
    }



    public function insertFullServiceCharge($id, $meter, $amountdue, $totalpaid)
    {
      $query = $this->db->query("SELECT * FROM  `service_charges` WHERE `meter_no` = '$meter'");
      if ($query->num_rows() >= 1){
        return false;
      }else{
        $query = $this->db->query("INSERT INTO `service_charges` (`user_id`, `meter_no`, `amount_due`, `total_paid`) VALUES ('$id', '$meter', '$amountdue', '$totalpaid')");
        return $query;
      }
      
    }

    public function getDetails($id)
    {
      $query = $this->db->query("SELECT * FROM `service_charges` WHERE `meter_no` = '$id'");
      $result = $query->result();
      return $result;
    }

    public function getSecurityDetails($id)
    {
      $query = $this->db->query("SELECT * FROM `security_levy` WHERE `meter_no` = '$id'");
      $result = $query->result();
      return $result;
    }

    public function getRefuseBinDetails($id)
    {
      $query = $this->db->query("SELECT * FROM `refuse_bin_levy` WHERE `meter_no` = '$id'");
      $result = $query->result();
      return $result;
    }

    public function getPowerDetails($id)
    {
      $query = $this->db->query("SELECT * FROM `power_payment` WHERE `meter_no` = '$id'");
      $result = $query->result();
      return $result;
    }

    public function getInfrastructureDetails($id)
    {
      $query = $this->db->query("SELECT * FROM `infrastructure_levy` WHERE `meter_no` = '$id'");
      $result = $query->result();
      return $result;
    }
    

    public function updateServiceCharge($meter_no, $amount, $totalpaid, $advance_month, $updated_date)
    {
      $query = $this->db->query("UPDATE `service_charges` SET `amount_due` = '$amount', `total_paid` = '$totalpaid', `advance_month` = '$advance_month', `date_created` = '$updated_date' WHERE `meter_no` = '$meter_no'");
      return $query;
    }


    public function updateSecurityCharge($meter_no, $amount, $totalpaid, $updated_date)
    {
      $query = $this->db->query("UPDATE `security_levy` SET `amount_due` = '$amount', `total_paid` = '$totalpaid', `date_created` = '$updated_date' WHERE `meter_no` = '$meter_no'");
      return $query;
    }

    public function updateRefuseBinCharge($meter_no, $amount, $totalpaid, $updated_date)
    {
      $query = $this->db->query("UPDATE `refuse_bin_levy` SET `amount_due` = '$amount', `total_paid` = '$totalpaid', `date_created` = '$updated_date' WHERE `meter_no` = '$meter_no'");
      return $query;
    }

    public function updatePowerPayment($meter_no, $amount, $totalpaid, $updated_date)
    {
      $query = $this->db->query("UPDATE `power_payment` SET `amount_due` = '$amount', `total_paid` = '$totalpaid', `date_created` = '$updated_date' WHERE `meter_no` = '$meter_no'");
      return $query;
    }

    public function updateInfrastructurePayment($meter_no, $amount, $totalpaid, $updated_date)
    {
      $query = $this->db->query("UPDATE `infrastructure_levy` SET `amount_due` = '$amount', `total_paid` = '$totalpaid', `date_created` = '$updated_date' WHERE `meter_no` = '$meter_no'");
      return $query;
    }


    public function editServiceCharge($meter_no, $amount, $totalpaid)
    {
      $query = $this->db->query("UPDATE `service_charges` SET `amount_due` = '$amount', `total_paid` = '$totalpaid' WHERE `meter_no` = '$meter_no'");
      return $query;
    }

    public function getServiceCharge($meter)
    {
      $query = $this->db->query("SELECT * FROM `service_charges` WHERE `meter_no` = '$meter'");
      if($query->num_rows() > 0){
        $result = $query->result();
        return $result;
      }else{
        return false;
      }
    }

    public function getAmountDueByProId($id)
    {
      $query = $this->db->query("SELECT * FROM `service_charges` WHERE `product_id` = '$id'");
      if($query->num_rows() > 0){
        $result = $query->result();
        return $result;
      }else{
        return false;
      }
    }


    public function getServiceChargeByProId($id)
    {
      $query = $this->db->query("SELECT * FROM `product` WHERE `id` = '$id'");
      if($query->num_rows() >= 1){
        $result = $query->result();
        return $result;
      }else{
        return false;
      }
    }

    public function updateServiceChargeByProId($meter_no, $id, $amount){
      $query = $this->db->query("UPDATE `service_charges` SET `amount_due` = '$amount' WHERE `product_id` = '$id' and  `meter_no` = '$meter_no'");
      return $query;
    }
 

}  
?>