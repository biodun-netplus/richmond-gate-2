<?php
class Outstanding_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function save($data)
    {
        $this->db->insert('outstanding_bills', $data);
        $lastId = $this->db->insert_id();
        return $lastId;

    }

    public function all()
    {
        $this->db->select('outstanding_bills.*,aauth_users.full_name')
            ->from('outstanding_bills')
            ->join('aauth_users', 'outstanding_bills.user_id = aauth_users.id')
            ->order_by('outstanding_bills.created_at', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

    public function get($id)
    {
        $this->db->select('*');
        $query = $this->db->get_where('outstanding_bills', array('id' => $id));
        return $query->row();
    }

    public function get_meter($id)
    {
        $this->db->select('meter_no');
        $query = $this->db->get_where('aauth_users', array('id' => $id));
        return $query->row();
    }

    public function user_outstanding($where)
    {
        $this->db->select('*');
        $query = $this->db->get_where('outstanding_bills', $where);
        return $query->result();
    }

    public function delete($id){
        $this->db->where('id',$id);
        $delete = $this->db->delete('outstanding_bills');
        return $delete;
    }

    public function update($data,$id){
        $this->db->where('id', $id);
        if ($this->db->update('outstanding_bills', $data)) {
            return true;
        }
        return false;
    }
}
