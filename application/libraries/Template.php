<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Template 
{
    var $ci;

    function __construct() 
    {
        $this->ci =& get_instance();
    }


    function load($template,$view,$param='') 
    {
        $data['body'] = $this->ci->load->view($view, $param, true);
        $this->ci->load->view('templates/'.$template,$data);
        
    }

}