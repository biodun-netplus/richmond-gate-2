<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Outstanding extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('Outstanding_model');
        $this->load->model('User_model');
        if (!$this->aauth->is_loggedin())
            return redirect('login', 'refresh');
        if (!$this->aauth->is_member('Admin')){
            show_error('Access Denied');
        }

    }

    public function index(){

        $data = array(
            'title' => 'Outstanding Payments'
        );

        $data['outstanding_payments'] =  $this->Outstanding_model->all();
        $this->template->load('default', 'outstanding/index',$data);
    }

    public function create(){
        $data = array(
            'title' => 'Create Outstanding'
        );
        $data['users'] = $this->User_model->user_type_record('Public');
        if(isset($_POST['create_o_bill'])){
            $this->form_validation->set_rules('description', 'Bill Title', 'required');
            $this->form_validation->set_rules('user', 'User', 'required');
            $this->form_validation->set_rules('outstanding_amount', 'Amount', 'required|numeric');
            if ($this->form_validation->run() == TRUE)
            {
                $user_id = $this->input->post('user',true);
                $meter = $this->Outstanding_model->get_meter($user_id);
                $meter_no = $meter->meter_no;
                $save_data = [
                    'description'=>$this->input->post('description',true),
                    'user_id'=>$this->input->post('user',true),
                    'amount'=>$this->input->post('outstanding_amount',true),
                    'meter_no' => $meter_no
                ];
                $outstanding = $this->Outstanding_model->save($save_data);
                if($outstanding)
                {
                    $this->session->set_flashdata('success', 'Outstanding bill created successfully.');
                    redirect('outstanding');exit;
                }
                $this->session->set_flashdata('errors', 'Outstanding bill could not be created.');
            }else{
                $errors = validation_errors();
                $this->session->set_flashdata('errors', $errors);
            }
        }
        $this->template->load('default', 'outstanding/create',$data);
    }

    public function delete($id){
        $outstanding = $this->Outstanding_model->get($id);
        if(!$outstanding){
            $this->session->set_flashdata('errors', 'Outstanding bill does not exist.');
        }
        if($outstanding){
            if($outstanding->status == 0){
                if($this->Outstanding_model->delete($id)){
                    $this->session->set_flashdata('success', 'Outstanding bill deleted successfully.');
                }
            }else{
                $this->session->set_flashdata('errors', 'Outstanding bill cannot be delete.');
            }
        }
        redirect('outstanding');
    }

}
