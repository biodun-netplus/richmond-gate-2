<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
        $this->load->model('welcome_model');
		$this->load->library('URLShorterner');
	}

	
	public function index()
	{
		$this->load->library('form_validation');
		$this->load->view('welcome');
	}
	public function link()
	{
		$this->load->library('form_validation');
		$data = array(
			'item' => $this->input->post('item'),
			'price' => $this->input->post('price'),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone'),
			'name' => $this->input->post('name')
			);
		$result = $this->welcome_model->save($data);
		//create url;
		$url = base_url('welcome/makePayment/'.$result);
		// $data['res'] = $this->urlshorterner->shorten($url);
		$data['res'] = $this->input->post('email');
		$config = array(
			'mailtype' => 'html',
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_user' => 'saddle@netplusadvisory.com',
			'smtp_pass' => 'Saddle7890',
			'smtp_port' => 465,
			'priority' => 1,
			'newline' => "\r\n"
		);
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from('orders@netpluspay.com', 'Orders');
		$this->email->to($this->input->post('email'));

		$this->email->subject('Your Order');
		$this->email->message($url);

		$r = $this->email->send();
		
		if (!$r)
  		var_dump($this->email->print_debugger());
		$this->load->view('response', $data);
	}
	public function makePayment($id)
	{
		$data['details'] = $this->welcome_model->get($id);
		$this->load->view('pay', $data);

	}
	public function back()
	{
        var_dump($_REQUEST);
        exit;
		
		$data['result'] = $_REQUEST;
		$this->load->view('result', $data);
		
	}
}
