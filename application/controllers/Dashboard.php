<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('payment_model');
        $this->load->model('user_model');
		 $this->load->library('cart');
        if (!$this->aauth->is_loggedin())
            redirect('login', 'refresh');
    }

    public function index()
    {
        $payments = 0;
        $total_payments = 0;
        $last_payment = 0;
        $users = 0;
        $c_user = $this->aauth->get_user();
        $recent_payments = array();
        $outstanding_payments = array();
        $product = '';
		$dueAmount = '';
        $userList = '';
        $duePower = '';
		 
		$type = '';
        if ($this->aauth->is_member('Merchant')) {
			

            $payments = $this->db->where('user_id', $c_user->id)->where('status')->count_all_results('payments');
            $query = $this->db->select_sum('amount_paid')->where('user_id', $c_user->id)->where('status','Paid')->get('payments');
            $row = $query->row();
            $total_payments = $row->amount_paid;

            $query = $this->db->where('user_id', $c_user->id)->where('amount_paid !=', '')->where('status','Paid')->limit(1)->order_by('date_created', 'desc')->get('payments');
            $row = $query->row();
            if($row){
                $last_payment = $row->amount_paid;
            }


            $query = $this->db->where('user_id', $c_user->id)->limit(3)->get('payments');
            $recent_payments = $query->result();
        }

        if ($this->aauth->is_member('Admin')) {
			
            $payments = $this->db->where('status', 'Paid')->count_all_results('payments');
            $query = $this->db->select_sum('amount_paid')->where('status','Paid')->where('payment_type','Card')->get('payments');
            $outstanding = $this->db->select_sum('amount')->where('status','1')->get('outstanding_bills');
            $bills = $outstanding->row();
            $total_bill_paid = 0;
            $total_other_payments = 0;
            $row = $query->row();
            $total_payments = $row->amount_paid;
            $users = count($this->user_model->regular_user());


            //$users = count($this->aauth->list_users());
            $outstanding_payments = $this->payment_model->outstanding(5);
            $recent_payments = $this->payment_model->all(5);
			$userList =  $ProPay = $this->db->query("SELECT * FROM  aauth_users ORDER BY  id DESC LIMIT 0 , 5")->result();
        }
		
		if ($this->aauth->is_member('Public')) {
		
             $user = $this->aauth->get_user();	
             
             $meter_no = $user->meter_no;

			$month_start = strtotime('first day of this month', time());
            $month_end = strtotime('last day of this month', time());
			$startDate =  date('Y-m-d', $month_start).' 00:00:00';
            $endDate = date('Y-m-d', $month_end).' 23:59:59';

             $product = $this->db->query("SELECT * FROM product where partner_type='".$user->partner_type."'")->result();
		     $this->db->where('meter_no',$meter_no);
             $payments = $this->db->count_all_results('payments');
             
             $where = "date_created BETWEEN '".$startDate."' and '". $endDate."'";
             //$query = $this->db->select_sum('amount')->where('status','Paid')->where('user_id',$user->id)->where('type','Security Charge')->where($where)->get('payments');
            //  $query = $this->db->select_sum('amount')->where('status','Paid')->where('meter_no',$meter_no)->where('type','Security Charge')->where($where)->get('payments');
            //  $row = $query->row();
            // if($row->amount !== NULL){
            //     $total_security_payments = $row->amount;
            // }else{
            //     $total_security_payments = 0;
            // }

            //outstanding bills
            $query = $this->db->select_sum('amount')->where('status', '0')->where('user_id',$user->id)->get('outstanding_bills');
            $row = $query->row();
            if($row->amount !== NULL){
                $total_outstanding_bill = $row->amount;
            }else{
                $total_outstanding_bill = 0;
            }
         
            //outstanding bills payed
            $query = $this->db->select_sum('amount')->where('status', '1')->where('user_id',$user->id)->get('outstanding_bills');
            $row = $query->row();
            if($row->amount !== NULL){
                $total_bill_paid = $row->amount;
                if($total_outstanding_bill > 0){
                    $total_outstanding_bill = $total_outstanding_bill - $row->amount;
                }

            }else{
                $total_bill_paid = 0;
            }
    
             $query = $this->db->select_sum('amount')->where('status','Paid')->where('meter_no',$meter_no)->where('type','Service')->get('payments');
             $row = $query->row();
            if($row->amount !== NULL){
                $total_service_payments = $row->amount;
            }else{
                $total_service_payments = 0;
            }

             $query = $this->db->select_sum('amount')->where('status','Paid')->where('meter_no',$meter_no)->where('type','Refuse Bin and Fire Ext')->get('payments');
             $row = $query->row();
            if($row->amount !== NULL){
                $total_refusebin_payments = $row->amount;
            }else{
                $total_refusebin_payments = 0;
            }
        

            $query = $this->db->select_sum('amount')->where('status','Paid')->where('meter_no',$meter_no)->where('type','Security Charge')->get('payments');
            $row = $query->row();
            if($row->amount !== NULL){
                $total_security_payments = $row->amount;
            }else{
                $total_security_payments = 0;
            }

            $query = $this->db->select_sum('amount')->where('status','Paid')->where('meter_no',$meter_no)->where('type','Infrastructure')->get('payments');
            $row = $query->row();
            if($row->amount !== NULL){
                $total_infrastructure_payments = $row->amount;
            }else{
                $total_infrastructure_payments = 0;
            }

            $query = $this->db->select_sum('amount')->where('status','Paid')->where('meter_no',$meter_no)->where('type','Power Payment')->get('payments');
            $row = $query->row();
            if($row->amount !== NULL){
                $total_power_payments = $row->amount;
            }else{
                $total_power_payments = 0;
            }
            
            $query = $this->db->select_sum('amount')->where('status','Paid')->where('meter_no',$meter_no)->where('type','Other')->get('payments');
            $row = $query->row();
            if($row->amount !== NULL){
                $total_other_payments = $row->amount;
            }else{
                $total_other_payments = 0;
            }
            
            
            $total_payments = $total_power_payments + $total_security_payments  + $total_refusebin_payments + $total_infrastructure_payments + $total_other_payments + $total_service_payments;
        

            // $TotalAmountForServicePayment = $this->db->query("SELECT *
			// 											FROM service_charges
            //                                             WHERE meter_no = '".$meter_no."'")->row();
            
            // if(!isset($TotalAmountForServicePayment->amount_due)){
            //     $dueServiceAmount = 0;
            // }else{
            //     $dueServiceAmount = $TotalAmountForServicePayment->amount_due;
            // }
  
            // if($serviceChargePayment >= $dueServiceAmount){
            //     $dueServiceAmount = $serviceChargePayment;
            // 

            // $query = $this->db->query("SELECT `advance_payment` FROM `service_charges` WHERE `meter_no` = '$meter_no'");
            // $row = $query->row();
            // if($row != NULL){
            //     $advance_pay = (int)$row->advance_payment;
            //     if($advance_pay > 0){
            //         $dueServiceAmount  = 0;
            //     }
            // }  

           
             // Total amount due Water
            //  $TotalAmountForServicePayment = $this->db->query("SELECT sum(product_price) as amountTotal
            //  FROM product
            //  WHERE product_type='Refuse Bin and Fire Ext' AND partner_type = '" . $user->partner_type . "' AND property_type = '" . $user->type_of_property . "'")->row();
            // $dueWaterAmount = 0;
            //$dueWaterAmount = $TotalAmountForServicePayment->amountTotal;

            // Total amount security
            // $TotalAmountForServicePayment = $this->db->query("SELECT sum(product_price) as amountTotal
            // FROM product
            // WHERE product_type='Security Charge' AND partner_type = '" . $user->partner_type . "' AND property_type = '" . $user->type_of_property . "'")->row();
            // $dueSecurityAmount = 0;
            //$dueSecurityAmount = $TotalAmountForServicePayment->amountTotal;
            
			//$dueAmount =  (($total_payments - $TotalAmountForPayment) > 0 ? 0 : $TotalAmountForPayment - $total_payments);
            $users = count($this->aauth->list_users('Merchant'));
            $recent_payments = $this->payment_model->all(3);
            $outstanding = $this->payment_model->outstanding(3);
           
           // $dueAmount =  $dueServiceAmount + $dueWaterAmount + $dueSecurityAmount + $total_outstanding_bill;
     
       
            // $dueAmount = $dueAmount - $total_payments;
           
            // if($total_payments > $dueAmount){
            //     $dueAmount = $dueWaterAmount + $dueSecurityAmount;
            // }

            // if($total_refusebin_payments > 0 ){
            //     $dueAmount = $dueAmount - $total_refusebin_payments ;
            // }

            // if($total_security_payments > 0){
            //     $dueAmount = $dueAmount - $total_security_payments;
            // }

            // if($total_power_payments > 0){
            //     $dueAmount = $dueAmount - $total_power_payments;
            // }
         
            // if($total_outstanding_bill > 0){
            //     $dueAmount = $dueAmount - $total_outstanding_bill;
            // }
          
            

            //Infrastructure
            $InfrastructureChargeDue = $this->db->query("SELECT * FROM infrastructure_levy WHERE meter_no = '$meter_no'")->row();
            if($InfrastructureChargeDue !== NULL){
                $dueInfrastructureChargeAmount = $InfrastructureChargeDue->amount_due;
            }else{
                $dueInfrastructureChargeAmount = 0;
            }
           

            //Refuse Bin
            $refusebinChargeDue = $this->db->query("SELECT * FROM refuse_bin_levy WHERE meter_no = '$meter_no'")->row();
            if($refusebinChargeDue !== NULL){
                $dueRefuseBinAmount = $refusebinChargeDue->amount_due;
            }else{
                $dueRefuseBinAmount = 0;
            }
           

            // Security Infrastructure
            $securityChargeDue = $this->db->query("SELECT * FROM security_levy WHERE meter_no = '$meter_no'")->row();
            if($securityChargeDue !== NULL){
                $dueSercurityChargeAmount = $securityChargeDue->amount_due;
            }else{
                $dueSercurityChargeAmount = 0;
            }
            

            //Service Charge
            $serviceChargeDue = $this->db->query("SELECT * FROM service_charges WHERE meter_no = '$meter_no'")->row();
            if($serviceChargeDue !== NULL){
                $dueServiceCharegAmount = $serviceChargeDue->amount_due;
            }else{
                $dueServiceCharegAmount = 0;
            }
            

            //Power Due
            $PowerDue = $this->db->query("SELECT * FROM power_payment WHERE meter_no = '$meter_no'")->row();
          
            if($PowerDue !== NULL){
                $duePowerAmount = $PowerDue->amount_due;
            }else{
                $duePowerAmount = 0;
            }
          
         
        }

       

        if ($this->aauth->is_member('Finance')) {
            $data['title'] = 'Dashboard';
            $data['payments'] =  $this->payment_model->getAdminPayment();
        
            //Total number of transaction last month
            $data['counts'] =  $this->payment_model->getMonthlyCount();
            //Total count in the last month * 100
            $data['monthly'] = $data['counts'] * 100;
            
            $data['totals'] =  $this->payment_model->getAdminPowerPayment();
        }else if($this->aauth->is_member('Admin')){
            $data = array(
                'title' => 'Dashboard',
                'payments' => $payments,
                'total_payments' => $total_payments + $total_bill_paid +  $total_other_payments,
                'last_payment' => $last_payment,
                'users' => $users,
                'recent_payments' => $recent_payments,
                'outstanding_payments' => $outstanding_payments,
                'product'=>$product ,
                'userList' =>$userList
            );
        }else{

            $dueAmount = $duePowerAmount + $dueServiceCharegAmount + $dueSercurityChargeAmount +  $dueRefuseBinAmount +$dueInfrastructureChargeAmount + $total_outstanding_bill;
            $data = array(
                'title' => 'Dashboard',
                'payments' => $payments,
                'total_payments' => $total_payments + $total_bill_paid +  $total_other_payments,
                'last_payment' => $last_payment,
                'users' => $users,
                'recent_payments' => $recent_payments,
                'outstanding_payments' => $outstanding_payments,
                'product'=>$product ,
                'dueAmount' =>$dueAmount,
                'duePowerAmount' => $duePowerAmount,
                'dueServiceCharegAmount' => $dueServiceCharegAmount,
                'dueSercurityChargeAmount' => $dueSercurityChargeAmount,
                'dueRefuseBinAmount' => $dueRefuseBinAmount,
                'dueInfrastructureChargeAmount' => $dueInfrastructureChargeAmount,
                'userList' =>$userList
            );
        }
    
		
        $this->template->load('default', 'account/dashboard', $data);

    }


    public function profile()
    {

        $data = array(
            'title' => 'Profile - Settings'
        );

        $this->template->load('default', 'account/settings', $data);

    }

    public function change_password()
    {

        $this->form_validation->set_rules('current_password', 'Current Password', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');

        $user = $this->aauth->get_user();

        if ($this->form_validation->run() == TRUE) {
            $current_pass = $this->input->post('current_password');
            $new_pass = $this->input->post('password');

            $password_hash = $this->aauth->hash_password($current_pass, $user->id);

            if ($this->aauth->verify_password($password_hash, $user->pass)) {

                if ($this->aauth->update_user($user->id, $email = FALSE, $new_pass, $username = FALSE)) {
                    $this->session->set_flashdata('success', 'Password updated successfully.');

                    redirect('profile/settings', 'refresh');
                }

            } else {
                $this->session->set_flashdata('errors', 'Current password entered is incorrect.');
            }


        }

        $errors = (validation_errors() ? validation_errors() : ($this->aauth->get_errors() ? $this->aauth->get_errors() : $this->session->flashdata('errors')));

        $this->session->set_flashdata('errors', $errors);

        redirect('profile/settings', 'refresh');

    }


    public function profile_update()
    {

        $c_user = $this->aauth->get_user();


        $this->form_validation->set_rules('fullname', 'Full Name', 'required');
        if ($this->aauth->is_member('Merchant')) {
           // $this->form_validation->set_rules('meter_no', 'Meter No', 'required');
			$this->form_validation->set_rules('mobile_no', 'Mobile No', 'required');
			$this->form_validation->set_rules('house_address', 'House Address', 'required');
			$this->form_validation->set_rules('type_of_property', 'Type of property', 'required');
        }
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');

        $userid = $c_user->id;


        if ($this->form_validation->run() == TRUE) {


            if (!empty($this->input->post('password', true))) {
                $pass = $this->input->post('password', true);
            } else {
                $pass = FALSE;
            }

            if ($c_user->email == $this->input->post('email', true)) {
                $email = FALSE;
            } else {
                $email = $this->input->post('email', true);
            }


            if ($c_user->email != $this->input->post('email', true)) {
                if ($this->aauth->update_user($userid, $email, $pass) === FALSE) {

                    $this->session->set_flashdata('errors', $this->aauth->get_errors());
                    return redirect('profile/settings');

                }
            }

            $meter_no = $this->input->post('meter_no', true);

            $additional_data = array(
                'meter_no' => $meter_no,
                'full_name' => $this->input->post('fullname', true),
				'mobile_no' => $this->input->post('mobile_no', true),
				'house_address' => $this->input->post('house_address', true),
				'type_of_property' => $this->input->post('type_of_property', true),
				'cug_no' => $this->input->post('cug_no', true),
            ); 

            $this->aauth->aauth_db->where('id', $userid);

            if ($this->aauth->aauth_db->update('aauth_users', $additional_data)) {
                $this->session->set_flashdata('success', 'Your account has been updated successfully.');
            }

            return redirect('profile/settings', 'refresh');
        }

        $errors = (validation_errors() ? validation_errors() : ($this->aauth->get_errors() ? $this->aauth->get_errors() : $this->session->flashdata('errors')));

        $this->session->set_flashdata('errors', $errors);
        return redirect('profile/settings');
    }


    public function logout()
    {

        $this->aauth->logout();

        redirect('login', 'refresh');
    }

}