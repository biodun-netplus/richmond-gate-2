  <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Fed Eats</title>

    <!-- Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <!-- <link href="css/style.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet"> -->

    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
    
  </head>
  <body>

    <style type="text/css">
    	form { margin: 0px 10px !important; }

h2 {
  margin-top: 2px!important;
  margin-bottom: 2px;
}

.container { max-width: 360px!important; }

.divider {
  text-align: center!important;
  margin-top: 20px!important;
  margin-bottom: 5px!important;
}

.divider hr {
  margin: 7px 0px!important;
  width: 35%!important;
}

.left { float: left!important; }

.right { float: right!important; }

.form-group h2 {
	font-size: 20px;
	text-align: center;
}

.form-group label {
	font-size: 12px;
	font-weight: 300;
}

form p {
	font-size: 10px;
	text-align: center;
	padding-bottom: 5px;
}

hr {
	margin-top: 10px;
	margin-bottom: 10px;
}

.panel-body {
	padding: 8px!important;
}



    </style>


    <div class="container">
		<div class="row">
			<div class="panel panel-primary">
				<div class="panel-body">
					
        			<span style="color:red"></span>

      				<?php echo form_open_multipart('welcome/link'); ?>
						<div class="form-group">
							<h2>Take Order</h2>
						</div>
						<div class="form-group">
							<label class="control-label" for="fullname">Your Order</label>
							<input id="fullname" name="item" type="text" maxlength="150" class="form-control">
						</div>
						<div class="form-group">
							<label class="control-label" for="email">Name</label>
							<input id="email" name="name" type="text" maxlength="50" class="form-control">
						</div>
						<div class="form-group">
							<label class="control-label" for="email">Email</label>
							<input id="email" name="email" type="email" maxlength="50" class="form-control">
						</div>
						<div class="form-group">
							<label class="control-label" for="businessname">Price</label>
							<input id="businessname" name="price" type="text" maxlength="50" class="form-control">
						</div>
						<div class="form-group">
							<label class="control-label" for="password">Phone</label>
							<input id="password" name="phone" type="text" maxlength="25" class="form-control" placeholder="" length="40">
						</div>
						
						<div class="form-group">
							<button id="signupSubmit" type="submit" class="btn btn-info btn-block">Place Order</button>
						</div>
						<p class="form-group"></p>
						<hr>
						<!-- <p>Already have an account? <a href="#">Sign in</a></p> -->
					</form>
				</div>
			</div>
		</div>
	</div>