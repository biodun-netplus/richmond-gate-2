<div class="container" id="registration-form">
    <div class="row">
        <div class="col-md-7 col-xs-12 hidden-xs login-heading">
            <div>
                <p>Welcome to</p>
            </div>
            <div>
                <p style="background: #071c0e;">Richmond Gate Phase 2 Estate residents and</p>
            </div>
            <div>
                <p style="background:#324a17;">utilities management portal</p>
            </div>
        </div>
        <div class="col-md-5 col-xs-12">
            <div align="center">
                <img src="https://netpluspay.com/images/netpluspayLogoGreen.png" class="login-logo" style="height: 50px; width: 200px;"/>
                <br/>
            </div>            <?php $this->load->view('includes/notification'); ?>
            <div class="panel login-panel">
           
                <form id="validate-form" class="form-horizontal"  method="post" data-parsley-validate="" "autocomplete"="off" accept-charset="utf-8">


                <div class="panel-body">
                <div class="form-group mb-md">
                        <div class="col-xs-8 col-xs-offset-2">
                            <input type="text" class="form-control" name="firstname" id="FirsttName"
                                   placeholder="First Name" required value="<?php echo set_value('firstname'); ?>" >
                        </div>

                    </div>

                    <div class="form-group mb-md">
                        <div class="col-xs-8 col-xs-offset-2">
                            <input type="text" class="form-control" name="lastname" id="LasttName"
                                   placeholder="Last Name" required value="<?php echo set_value('lastname'); ?>" >
                        </div>

                    </div>
                    <div class="form-group mb-md">
                        <div class="col-xs-8 col-xs-offset-2">
                            <input type="text" class="form-control" name="meter_no" id="metterno"
                                   placeholder="Meter Number"   value="<?php echo set_value('meter_no'); ?>">
                        </div>
                    </div>
                    
                    <div class="form-group mb-md">
                        <div class="col-xs-8 col-xs-offset-2">
                            <input type="text" class="form-control" name="mobile_no" id="mobile_no"
                                   placeholder="Mobile Number" required value="<?php echo set_value('mobile_no'); ?>" >
                        </div>
                    </div>
                    
                     <div class="form-group mb-md">
                        <div class="col-xs-8 col-xs-offset-2">
                            <input type="text" class="form-control" name="cug_no" id="cug_no"
                                   placeholder="CUG Number"  value="<?php echo set_value('cug_no'); ?>" >
                        </div>
                    </div>
                    
                    <div class="form-group mb-md">
                        <div class="col-xs-8 col-xs-offset-2">
                           <select class="form-control" name="type_of_ownership" required >
                                <option>Ownership Type</option>
                                <option value="landlord">Landlord</option>
                                <option value="resident">Resident</option>
                            </select>
                        </div>
                    </div>
                    
                    
                    <div class="form-group mb-md">
                        <div class="col-xs-8 col-xs-offset-2">
                            <input type="text" class="form-control" name="house_address" id="house_address"
                                   placeholder="House Address" required value="<?php echo set_value('house_address'); ?>" >
                        </div>
                    </div>
                    
                    
                    <div class="form-group mb-md">
                        <div class="col-xs-8 col-xs-offset-2">
                           <select class="form-control" name="type_of_property" required >
                                <option>Select Property</option>
                                <option value="2BRStudioApartment">(2-BR) Studio Apartment</option>
                                <option value="Flat">Flats (3-BR)</option>
                                <option value="Maisonettes">Maisonettes (4-BR)</option>
                                <option value="Townhouses">Town Houses (4-BR)</option>
                                <option value="4BRQuads">Quads (4-BR)</option>
                                <option value="5BRQuads">Quads (5-BR)</option>
                               
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group mb-md">
                        <div class="col-xs-8 col-xs-offset-2">
                            <input type="text" class="form-control" required data-parsley-type="email"  name="email" id="Email" placeholder="Email" value="<?php echo set_value('email'); ?>">
                        </div>
                    </div>
                    <div class="form-group mb-md">
                        <div class="col-xs-8 col-xs-offset-2">
                            <input type="password" class="form-control" name="password" id="Password"
                                   placeholder="Password" data-parsley-minlength="8"  required>
                        </div>
                    </div>
                    <div class="form-group mb-md">
                        <div class="col-xs-8 col-xs-offset-2">
                            <input type="password" class="form-control" name="confirm_password" id="ConfirmPassword"
                                   placeholder="Confirm Password" data-parsley-minlength="8"  data-parsley-equalto="#Password" required>
                        </div>
                    </div>
               
                    <div class="form-group mb-n">
                        <div class="col-xs-offset-2 col-xs-8">
                            <div class="checkbox checkbox-inline checkbox-primary">
                                <label>
                                    <input type="checkbox" name="terms"  required />&nbsp;&nbsp;I accept the user
                                        agreement
                                </label>
                            </div>

                        </div>
                    </div>


                </div>
                <div class="panel-footer">
                    <div class="clearfix">
                        <button type="submit" class="btn btn-login btn-raised pull-left">Register</button>
                        <a href="<?php echo site_url('/'); ?>" class="btn btn-default pull-right">Already Registered?
                            Login</a>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
