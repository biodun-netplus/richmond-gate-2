<div class="static-content">
    <div class="page-content">
        <?php if ($this->aauth->is_member('Merchant')):?>
            <span class="pull-right" style="padding: 10px;">Meter No : <?= $this->aauth->get_user()->meter_no ?></span>
        <?php endif; ?>
        
        <div class="page-heading">
            <h1>Edit Power Payment</h1>

            <div class="options">

            </div>
        </div>
        <div class="container-fluid">
		<?php if(!empty($this->session->flashdata('flashMsg'))){?>
                <div class="alert alert-success">
                    <button class="close" data-close="alert"></button>
                    <span> <?php echo $this->session->flashdata('flashMsg')?></span>
                </div>
           <?php } ?>

            <div data-widget-group="group1">

                <div class="col-md-12">
                    <div id="form-errors" class="row"></div>

                    <div id="customer-info" class="row">
                    
                    <form action="<?php echo base_url().'shopping/addpowerpayment';?>" method="post" enctype="multipart/form-data">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Full Name</label>
                                <input class="form-control" required="required" type="text" name="full_name"  value="<?=$full_name; ?>">
                                <input class="form-control" required="required" type="hidden" name="meter_no"  value="<?=$meter_no; ?>">
                            </div>
                        </div>
                        
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Amount Due</label>
                                <input class="form-control"  required="required" type="number" name="amount_due" value="<?= $amount_due; ?>">
                            </div>
                        </div>
                        
                        
                        <div class="col-sm-12 center">
                            <div class="form-group">
                                <input type="submit" name="update" value="Update" class="btn btn-primary btn-raised pull-right">
                            </div>
                        </div>
                        </form> 

                    </div>
           

                </div>
                        
                            
            <div class="row">
                
            </div>

            </div>


        </div>
        <!-- .container-fluid -->
    </div>
    <!-- #page-content -->
</div>
                