<div class="static-content">
    <div class="page-content">
        <?php if ($this->aauth->is_member('Merchant')):?>
            <span class="pull-right" style="padding: 10px;">Meter No : <?= $this->aauth->get_user()->meter_no ?></span>
        <?php endif; ?>
        
        <div class="page-heading">
            <h1>Directory</h1>

            <?php /*?><div class="options">
      <form action="<?php echo site_url('users/regular_user') ?>" method="post">
     <input type="text" name="date_search" id="date_search">
     <input type="submit" name="Search" value="Search"> 
     </form>

            </div><?php */?>
        </div>
        <div class="container-fluid">
        <div class="row"> 
        
		<?php if(!empty($this->session->flashdata('flashMsg'))){?>
                <div class="alert alert-success">
                    <button class="close" data-close="alert"></button>
                    <span> <?php echo $this->session->flashdata('flashMsg')?></span>
                </div>
           <?php } ?>
         
         <div class="row">
          <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h2>Directory</h2>
                <div class="panel-ctrls"></div>
              </div>
              <div class="panel-body no-padding">
                <table id="defaultTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Phone</th>
                      <th>Meter No.</th>
                      <th>Cug No.</th>
                     <th>House Address</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
						$i=1;
						if(isset($records) && count($records)>0){
						foreach($records as $LoopRecord){ 
						if($LoopRecord->name !='Admin'){
						
						?>
                    <tr>
                      <td> <a href="<?php echo base_url() ?>users/profile/<?php echo $LoopRecord->id ?>"> <?php echo $LoopRecord->full_name ?></a></td>
                      <td> <?php echo $LoopRecord->email ?></td>
                      <td> <?php echo $LoopRecord->mobile_no ?></td>
                      <td> <?php echo $LoopRecord->meter_no ?></td>
                      <td> <?php echo $LoopRecord->cug_no ?></td>
                      <td> <?php echo $LoopRecord->house_address ?></td>
                     
                      
                    </tr>
                    <?php $i++;}}}?>
                  </tbody>
                </table>
              </div>
              <div class="panel-footer"></div>
            </div>
          </div>
        </div>
        
          

        </div>
        <!-- .container-fluid -->
    </div>
    <!-- #page-content -->
</div>     