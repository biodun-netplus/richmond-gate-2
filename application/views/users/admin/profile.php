<div class="static-content">
    <div class="page-content">
        <?php if ($this->aauth->is_member('Merchant')):?>
            <span class="pull-right" style="padding: 10px;">Meter No : <?= $this->aauth->get_user()->meter_no ?></span>
        <?php endif; ?>
        
        <div class="page-heading">
            <h1>User Profile</h1>

            
        </div>
        <div class="container-fluid">
        <div class="row"> 
        
		<?php if(!empty($this->session->flashdata('flashMsg'))){?>
                <div class="alert alert-success">
                    <button class="close" data-close="alert"></button>
                    <span> <?php echo $this->session->flashdata('flashMsg')?></span>
                </div>
           <?php } ?>
        
         <div class="row">
          <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h2>Profile</h2>
                <div class="panel-ctrls"></div>
              </div>
             
              <div class="panel-body no-padding">
              
              
              
                <table  class="table table-striped table-bordered" cellspacing="0" width="50%">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <td><?php echo $records['full_name']?></td>
                      
                    </tr>
                    
                    <tr>
                      <th>Email</th>
                      <td><?php echo $records['email']?></td>
                      
                    </tr>
                    <tr>
                      <th>Mobile Number</th>
                      <td><?php echo $records['mobile_no']?></td>
                      
                    </tr>
                    <tr>
                      <th>Meter Number</th>
                      <td><?php echo $records['meter_no']?></td>
                      
                    </tr>
                    <tr>
                      <th>Address</th>
                      <td><?php echo $records['house_address']?></td>
                      
                    </tr>
                    <tr>
                      <th>Type Of Property</th>
                      <td><?php echo $records['type_of_property']?></td>
                      
                    </tr>
                    <tr>
                      <th>Partner Type</th>
                      <td><?php echo $records['partner_type']?></td>
                      
                    </tr>
                    <tr>
                      <th>Registration Date</th>
                      <td><?php echo date('Y-M-d',strtotime($records['date_created']));?></td>
                      
                    </tr>
                   
                  </thead>
                  <tbody>
                    
                  </tbody>
                </table>
              </div>
              <div class="panel-footer"></div>
            </div>
          </div>
        </div>
        
        
        </div>
        <!-- .container-fluid -->
    </div>
    <!-- #page-content -->
</div>
                