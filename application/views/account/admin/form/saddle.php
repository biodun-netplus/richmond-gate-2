
<div class="col-sm-6">
    <div class="form-group">
        <label>Saddle Client ID</label>
        <input class="form-control" data-error="Please input saddle client id" id="inputPassword" placeholder="Client ID" required="required" type="text" name="saddle_client_id" value="<?php echo $saddle_client_id; ?>">
        <div class="help-block form-text with-errors form-control-feedback"></div>
    </div>
</div>
<div class="col-sm-6">
    <div class="form-group">
        <label>Saddle Client Secret</label>
        <input class="form-control" data-error="Please input saddle client secret" placeholder="Client Secret" required="required" type="text" name="saddle_client_secret" value="<?php echo $saddle_client_secret; ?>" >
        <div class="help-block form-text with-errors form-control-feedback"></div>
    </div>
</div>

<div class="col-sm-6">
    <div class="form-group">
        <label>Pickup Address</label>
        <input class="form-control" data-error="Please input pickup address" placeholder="Pickup Address" required="required" type="text" name="saddle_pickup_address" value="<?php echo $saddle_pickup_address; ?>">
        <div class="help-block form-text with-errors form-control-feedback"></div>
    </div>
</div>
<div class="col-sm-6">
    <div class="form-group">
        <label>Pickup Location</label>
        <input class="form-control" data-error="Please input pickup location" placeholder="Pickup Location" required="required" type="text" name="saddle_pickup_location" value="<?php echo $saddle_pickup_location; ?>" >
        <div class="help-block form-text with-errors form-control-feedback"></div>
    </div>
</div>

<div class="col-sm-6">
    <div class="form-group">
        <label>Pickup Contact Name</label>
        <input class="form-control" data-error="Please input saddle pickup contact name" placeholder="Pickup Contact Name" required="required" type="text" name="saddle_pickup_contact_name" value="<?php echo $saddle_pickup_contact_name; ?>">
        <div class="help-block form-text with-errors form-control-feedback"></div>
    </div>
</div>
<div class="col-sm-6">
    <div class="form-group">
        <label>Pickup Contact Number</label>
        <input class="form-control" data-error="Please input saddle pickup contact phone" placeholder="Pickup Contact Phone" required="required" type="text" name="saddle_pickup_contact_phone" value="<?php echo $saddle_pickup_contact_phone; ?>" >
        <div class="help-block form-text with-errors form-control-feedback"></div>
    </div>
</div>
<div class="col-sm-6">
    <div class="form-group">
        <label>Pickup Contact Email</label>
        <input class="form-control" data-error="Please input saddle pickup contact email" placeholder="Pickup Contact Email" required="required" type="email" name="saddle_pickup_contact_email" value="<?php echo $saddle_pickup_contact_email; ?>" >
        <div class="help-block form-text with-errors form-control-feedback"></div>
    </div>
</div>
<div class="col-sm-6">
    <div class="form-group">
        <label>Default Delivery Amount</label>
        <input class="form-control" data-error="Please input saddle default delivery amount" placeholder="Default Delivery Amount" required="required" type="text" name="saddle_default_delivery_amount" value="<?php echo $default_delivery_amount; ?>" >
        <div class="help-block form-text with-errors form-control-feedback"></div>
    </div>
</div>
