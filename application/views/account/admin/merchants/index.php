 <div class="main">
                <div class="breadcrumb">
                    <a href="#">Administrator</a>
                    <span class="breadcrumb-devider">/</span>
                    <a href="<?php echo current_url() ?>">Merchant</a>
                </div>
                <div class="content">
                    <div class="panel">
                        <div class="content-header no-mg-top">
                            <i class="fa fa-newspaper-o"></i>
                            <div class="content-header-title">Merchants</div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <?php $this->load->view('includes/notification'); ?>
                                <div class="content-box">
                                    <div class="content-box-header">
                                        <div class="row">
                                            <div class="col-md-6 sm-max sm-text-center">
                                                <a class="btn btn-primary sm-max" href="<?php echo site_url('admin/merchant/new') ?>"><i class="fa fa-plus"></i> Add New Merchant</a>
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-search"></i></div>
                                                    <input class="form-control" type="text" placeholder="Search">
                                                </div>

                                            </div>
                                           
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">ID</th>
                                                    <th>Company Name</th>
                                                    <th>Netplus ID</th>
                                                    <th>Email</th>
                                                    <th>Phone</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php foreach($merchants as $merchant): ?>
                                                
                                                <tr>
                                                    <th class="text-center"><?php echo $merchant->id; ?></th>
                                                    <th><?php echo $merchant->company_name; ?></th>
                                                    <td><?php echo $merchant->net_plus_merchant_id; ?></td>
                                                    <td><?php echo $merchant->email; ?></td>
                                                    <td><?php echo $merchant->phone; ?></td>
                                                    <td><?php echo ($merchant->banned == 0) ? 'Active' : 'De-activated'; ?></td>
                                                   <td><a href="<?php echo site_url('admin/merchant/edit/'.$merchant->id) ?>"><i class="fa fa-pencil"></i></a><a href="<?php echo site_url('admin/merchant/delete/'.$merchant->id) ?>" onclick="return confirm('Are you sure you want to delete merchant?')"> <i class="fa fa-trash"></i></a></td>
                                                 </tr>
                                                <?php endforeach; ?>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="content-box-footer">
                                        <div class="row">
                                            <div class="col-md-12 sm-max">
                                                <ul class="pagination sm-mgtop-5 pull-right ">
                                                    <?php foreach ($links as $link) {
                                                        echo $link;
                                                    } ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>