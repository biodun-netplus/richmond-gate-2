
            <div class="main">
                <div class="breadcrumb">
                    <a href="#">Administrator</a>
                    <span class="breadcrumb-devider">/</span>
                    <a href="<?php echo site_url('admin/merchants') ?>">Merchant</a>
                    <span class="breadcrumb-devider">/</span>
                    <a href="<?php echo current_url() ?>">Create</a>
                </div>
                <div class="content">
                    <div class="panel">
                        <div class="content-header no-mg-top">
                            <i class="fa fa-newspaper-o"></i>
                            <div class="content-header-title">Create New Merchant</div>
                        </div>
                        
                        <div class="row" >
                            <div class="col-md-8" style="float:none;margin:auto;">
                                <?php $this->load->view('includes/notification'); ?>
								
                                <div class="content-box">
									<div class="content-box-header">
										<div class="box-title">New Merchant</div>
									</div>
									<?php
                                        $attributes = array( 'id' => 'form-validate');
                                        echo form_open('admin/merchant/create', $attributes);
                                    ?>    <div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label>Company Name</label>
													<input class="form-control" data-error="Please input conpany name" placeholder="Enter Company Name" required="required" type="text"  name="company_name">
													<div class="help-block form-text with-errors form-control-feedback"></div>
												</div>
											</div>
                                            <div class="col-sm-6">
												<div class="form-group">
													<label>Net Plus Merchant ID</label>
													<input class="form-control" data-error="Please input Net Plus Merchant ID" placeholder="Enter Net Plus Merchant ID" required="required" type="text"  name="merchant_id">
													<div class="help-block form-text with-errors form-control-feedback"></div>
												</div>
											</div>
											
                                            <div class="col-sm-6">
												<div class="form-group">
													<label>Email address</label>
                                                    <input class="form-control" data-error="Email address is invalid" placeholder="Enter Email" required="required" type="email"  name="email">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
												</div>
											</div>
                                            <div class="col-sm-6">
												<div class="form-group">
													<label>Contact Number</label>
													<input class="form-control" data-error="Please input Contact Number" placeholder="Enter Contact Number" required="required" type="text"  name="contact_no">
													<div class="help-block form-text with-errors form-control-feedback"></div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
                                                    <label>Password</label>
                                                    <input class="form-control" data-error="Please input user password" id="inputPassword" placeholder="Enter Password" required="required" type="password"  name="password">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
												</div>
											</div>
                                            <div class="col-sm-6">
												<div class="form-group">
                                                    <label>Confirm Password</label>
                                                    <input class="form-control" data-error="confirm password is required" placeholder="Confirm Password" name="confirm_password" data-match="#inputPassword" data-match-error="Whoops, password don't match" required="required" type="password">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
												</div>
											</div>
                                            
                                            <div class="col-sm-6">
												<div class="form-group">
													<label>Contact Address</label>
													<textarea class="form-control" rows="5" data-error="Please enter Contact Address" required="required"  name="address"></textarea>
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
												</div>
											</div>
                                            <div class="col-sm-6">
												<div class="form-group">
													<label>Enable Saddle Integration</label>
                                                    <select id="saddle_int" name="saddle_int" class="form-control" required="required" >
                                                        <option></option>
                                                        <option value="1">Yes</option>
                                                        <option value="0">No</option>
                                                    </select>
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
												</div>
											</div>
										</div>
                                        <div id="saddle" class="row">
                                            
                                        </div>
                                        
                                        
										<div class="content-box-footer">
											<button class="btn btn-primary" type="submit"><i class="fa fa-pencil"></i> Create</button>
										</div>
									</form>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<script type="application/javascript">
    $(document).ready(function(){
       $("#saddle_int").change(function(){
           var selected = $(this).val();
           //alert(selected);
           if(selected == 1){
            $.get("<?php echo site_url('admin/form/component/saddle'); ?>", function(data, status){
                
                $('#saddle').html(data);
                $('#form-validate').validator('update');
                //alert("Data: " + data + "\nStatus: " + status);
            });
           }else{
               $('#saddle').html('');
               $('#form-validate').validator('update');
           }
           
        });
 
    });
</script>