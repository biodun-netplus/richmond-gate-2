<div class="main">
    <div class="breadcrumb">
        <a href="#">Orders</a>
        <span class="breadcrumb-devider">/</span>
        <a href="#">Manage</a>
    </div>
    <div class="content">
        <div class="panel">
            <div class="content-header no-mg-top">
                <i class="fa fa-newspaper-o"></i>

                <div class="content-header-title">Order List</div>
            </div>
            <form method="get">
                <div class="row">
                    <div class="col-md-4">
                        <div class="content-box">
                            <div class="form-group">
                                <label for=""> Search Query</label>

                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-search"></i></div>
                                    <input class="form-control" name="filter_query" type="text"
                                           value="<?php echo $filter_query; ?>" placeholder="Search">
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="content-box">
                            <div class="form-group">
                                <label for=""> Filter Date</label>

                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    <input id="" class="multi-daterange form-control" name="filter_date" type="text"
                                           value="<?php echo $filter_date; ?>">
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="content-box">
                            <div class="form-group">
                                <label for=""> Filter By Status</label>

                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-cubes"></i></div>
                                    <select class="form-control" name="filter_status">
                                        <option value=""></option>
                                        <option
                                            value="Pending" <?php echo ($this->input->get('filter_status') == 'Pending') ? 'selected' : ''; ?>>
                                            Pending
                                        </option>
                                        <option
                                            value="Completed" <?php echo ($this->input->get('filter_status') == 'Completed') ? 'selected' : ''; ?>>
                                            Completed
                                        </option>
                                        <option
                                            value="Cancelled" <?php echo ($this->input->get('filter_status') == 'Cancelled') ? 'selected' : ''; ?>>
                                            Cancelled
                                        </option>
                                    </select>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 sm-max sm-text-center">
                        <div style="margin-top:10px;">
                            <a class="btn btn-warning pull-right" href="<?php echo base_url('order/list') ?>"
                               style="margin-left:10px;"><i class="fa fa-remove"></i> Reset</a>
                            <button class="btn btn-info sm-max  pull-right" type="submit"><i class="fa fa-search"></i>
                                Search
                            </button>


                        </div>
                    </div>

                </div>
            </form>
            <div class="row">
                <div class="col-md-12">
                    <?php $this->load->view('includes/notification'); ?>
                    <div class="content-box">
                        <div class="content-box-header">
                            <div class="row">
                                <?php if ($this->aauth->is_member('Merchant')): ?>
                                    <div class="col-md-6 sm-max sm-text-center">
                                        <a class="btn btn-primary sm-max" href="<?php echo site_url('order/new') ?>"><i
                                                class="fa fa-pencil"></i> Add New Order</a>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Order ID</th>
                                    <?php if ($this->aauth->is_member('Admin')): ?>
                                        <th>Merchant Account</th>
                                    <?php endif; ?>
                                    <th>Customer Name</th>
                                    <th>Email</th>
                                    <th>Order Date</th>
                                    <th>Status</th>
                                    <th>Order Total</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (count($orders) > 0): ?>

                                    <?php foreach ($orders as $order): ?>
                                        <tr>
                                            <th><?php echo $order->order_id; ?></th>
                                            <?php if ($this->aauth->is_member('Admin')): ?>
                                                <th><?php echo $order->company_name; ?></th>
                                            <?php endif; ?>
                                            <td><?php echo $order->customer_firstname . ' ' . $order->customer_lastname; ?></td>
                                            <td><?php echo $order->contact_email; ?></td>
                                            <td><?php echo $order->date_created; ?></td>
                                            <td><?php echo $order->status; ?></td>
                                            <td><?php echo '<b style="font-weight: bold;">' . $order->currency . '</b>' . number_format($order->amount, 2); ?></td>
                                            <td class="">
                                                <div class="order-actions">
                                                    <a href="<?php echo site_url('order/review/' . $order->order_id); ?>"><i
                                                            class="fa fa-eye"></i></a>
                                                    <a href="<?php echo site_url('order/delete/' . $order->id); ?>"
                                                       onclick="return confirm('Are you sure you want to delete order?')"><i
                                                            class="fa fa-trash"></i></a>
                                                    <?php if(($order->status != 'Completed') && ($order->status != 'Cancelled')):?>
                                                    <a href="<?php echo site_url('order/complete/' . $order->id); ?>"
                                                       onclick="return confirm('Are you sure you want mark order a completed?')"><i
                                                            class="fa fa-check-circle"></i></a>
                                                    <?php endif; ?>
                                                    <?php if(($order->status != 'Completed') && ($order->status != 'Cancelled')):?>

                                                    <a href="<?php echo site_url('order/cancel/' . $order->id); ?>"
                                                       onclick="return confirm('Are you sure you want cancel order?')"><i
                                                            class="fa fa-remove"></i></a>
                                                    <?php endif; ?>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>

                                <?php else: ?>
                                    <tr>
                                        <td colspan="8" class="text-center">No results found</td>
                                    </tr>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>

                        <div class="content-box-footer">
                            <div class="row">
                                <div class="col-md-12 sm-max">
                                    <ul class="pagination sm-mgtop-5 pull-right ">
                                        <?php foreach ($links as $link) {
                                            echo $link;
                                        } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#multi-daterange').daterangepicker({"startDate": "<?php echo $from ?>", "endDate": "<?php echo $to ?>"});
    });
</script>